# Description
#   basic hubot
#
# Configuration:
#   LIST_OF_ENV_VARS_TO_SET
#
# Commands:
#   hubot hello - <what the respond trigger does>
#   orly - <what the hear trigger does>
#
# Notes:
#   <optional notes required for the script>
#
# Author:
#   koyaman <koyaman@concentinc.jp>

cheerio = require 'cheerio'
request = require 'request'


module.exports = (robot) ->
  # チョイスくん
  robot.hear /(.+)からチョイス/, (msg) ->
    items = msg.match[1].split(/[　・、\s]+/)
    item = msg.random items
    msg.reply "無難に#{item}でしょ"

  # 妖怪さがすくん
  robot.respond /youkai(?: (\S+))?/, (msg) ->
    query = msg.match[1]
    # send HTTP request
    baseUrl = 'https://ja.wikipedia.org/wiki/%E6%97%A5%E6%9C%AC%E3%81%AE%E5%A6%96%E6%80%AA%E4%B8%80%E8%A6%A7'
    request baseUrl, (_, res) ->
      # parse response body
      $ = cheerio.load res.body
      youkaiList = []
      $('.wikitable a').each ->
        a = $ @
        url = baseUrl + a.attr('href')
        name = a.text()
        youkaiList.push { url, name }
      # filter youkaiList
      filtered = youkaiList.filter (c) ->
        if query? then c.name.match(new RegExp(query, 'i')) else true
      # format youkaiList
      message = filtered
        .map (c) ->
          "#{c.name} #{c.url}"
        .join '\n'
      msg.send message


  links = []

  robot.router.get '/hubot-homepage/', (req, res) ->
    li = links.map (i) ->
      """
        <li><a href="#{i.url}" target="_blank">#{i.text}</a></li>
      """
    .join '\n'
    html = """
      <html>
      <head><title>Links</title></head>
      <body>
        <h1>Links</h1>
        <ul>
        #{li}
        </ul>
      </body>
      </html>
    """
    res.type 'html'
    res.send html

  robot.respond /homepage li?st?/, (msg) ->
    msg.send links.map((i, index) -> "[#{index}] #{i.text} #{i.url}").join('\n')

  robot.respond /homepage add (.+) (https?:\/\/.+)/, (msg) ->
    text = msg.match[1]
    url = msg.match[2]
    item = { text, url }
    links.push item
    msg.send "added #{item.text} #{item.url}"

  robot.respond /homepage re?m(?:ove)? (\d+)/, (msg) ->
    index = msg.match[1]
    item = links.splice(index, 1)[0]
    msg.send "removed #{item.text} #{item.url}" if item?
