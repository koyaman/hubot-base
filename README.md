# hubot-base

basic hubot

See [`src/base.coffee`](src/base.coffee) for full documentation.

## Installation

In hubot project repo, run:

`npm install hubot-base --save`

Then add **hubot-base** to your `external-scripts.json`:

```json
[
  "hubot-base"
]
```

## Sample Interaction

```
user1>> hubot hello
hubot>> hello!
```

## NPM Module

https://www.npmjs.com/package/hubot-base
